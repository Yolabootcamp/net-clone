import React, { useState } from "react";

export default function LoginPage() {
  const [emailAddress, setEmailAddress] = useState("");
  const [password, setPassword] = useState("");

  const handleEmail = (event) => {
    setEmailAddress(event.target.value);
  };
  const handlePassword = (event) => {
    setPassword(event.target.value);
  };

  return (
    <div>
      <form>
        <input
          type="text"
          onChange={handleEmail}
          placeholder="Email"
          required
        />
        <input
          type="text"
          onChange={handlePassword}
          placeholder="Password"
          required
        />
      </form>
    </div>
  );
}
