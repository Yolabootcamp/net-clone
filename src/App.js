import { Route, Router, Switch } from "react-router";
import LoginPage from "./components/LoginPage/LoginPage";

export default function App() {
  return (
    <>
      <Router>
        <div>
          <Switch>
            <Route exact path="/" />
            <Route path="/login-page" component={LoginPage} />
          </Switch>
        </div>
      </Router>
    </>
  );
}
